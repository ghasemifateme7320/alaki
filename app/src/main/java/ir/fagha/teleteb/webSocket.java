package ir.fagha.teleteb;


import android.util.Log;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

public class webSocket {
    private static final String TAG = "webSocket";
   private static Socket mSocket = null;
    public static Socket getWebSocketClient(){
        if(mSocket ==null){
            try {
                mSocket = IO.socket("http://192.168.1.85:8081");
            } catch (URISyntaxException e) {
                Log.e(TAG, "onCreate: URISyntaxException e" + e);
            }
        }
        return mSocket;

    }


}
