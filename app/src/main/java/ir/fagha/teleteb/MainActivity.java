package ir.fagha.teleteb;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements View.OnKeyListener {
    private final String TAG = getClass().getSimpleName();
    Button login;
    Context context;
    EditText username,password;
    Socket finalMSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        finalMSocket = webSocket.getWebSocketClient();
        finalMSocket.connect();
        username = findViewById(R.id.name);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login);

        finalMSocket.on("loginFail", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context,"login failed!",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        finalMSocket.on("loginSuccess", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG, "call: loginSuccessResult" +args );
                Intent intent = new Intent(MainActivity.this,ChatActivity.class);
                startActivity(intent);

            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              login();
            }
        });

        password.setOnKeyListener(this);
        username.setOnKeyListener(this);

    }

    public void login(){
        String name = username.getText().toString();
        String pass = password.getText().toString();
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("username",name);
            jsonObject.put("password",pass);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.finalMSocket.emit("login",jsonObject);
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent event) {

        if (event.getAction() == KeyEvent.ACTION_DOWN)
        {
            switch (keyCode)
            {
                case KeyEvent.KEYCODE_DPAD_CENTER:
                case KeyEvent.KEYCODE_ENTER:
                    login();

                    return true;
                default:
                    break;
            }
        }
        return false;
    }



}
